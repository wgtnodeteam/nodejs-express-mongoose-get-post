const express = require('express')
const app = express()
var mongoose = require("mongoose");
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/nodetest");

var studentsSchema = new mongoose.Schema({
	s_id: Number,
	s_name: String
});

var marksSchema = new mongoose.Schema({
	school_id: Number,
	s_id: Number,
	score: Number,
	status: String
});

var detailsSchema = new mongoose.Schema({
	city: String,
	email_id: String,
	school_id: Number,
	accomplishment: String
});

var Students = mongoose.model("students", studentsSchema);
var Marks = mongoose.model("marks", marksSchema);
var Details = mongoose.model("details", detailsSchema);

app.get('/', function (req, res) {
	var students = new Students();
	Students.aggregate([
		{ $lookup:
				{
				from: 'marks',
				localField: 's_id',
				foreignField: 's_id',
				as: 'marks'
				}
		},
		{
			$unwind: {
				path: "$marks",
				preserveNullAndEmptyArrays: true
			}
		}, 
		{
			$lookup: {
				from: "details",
				localField: "marks.school_id",
				foreignField: "school_id",
				as: "marks.details",
			}
		}
	])
	.then(item => {
		console.log('@@Tes', item)
		res.send(item);
	})
	.catch(err => {
		res.status(400).send("unable to save to database");
	});
})

app.post('/', function (req, res) {
	console.log('@@Test', req.body)
	var students = new Students(req.body);
	var marks = new Marks(req.body);
	var details = new Details(req.body);
	students.save()
		.then(item => {
			marks.save()
			details.save()
			res.send("item saved to database");
		})
		.catch(err => {
			res.status(400).send("unable to save to database");
		});
})

app.listen(3000)